#include "bargraph.h"

//use regex to find the data file
//January 6th 2020

//get the index of the data file 
int get_filename_index (int argc, char** argv){
	if (argc >= 2){
//skip 0 because 0 is the name of this programs executable... and it passes the access test
		for (int i = 1 ; i < argc ; i++){
			if (access( argv[i] , F_OK) != -1 ) {
				// arg is a proper file
				printf("%s is a file!\n", argv[i]);
				return i;
			}
		}
	}
	printf("No files found!\n");
	return -1;
}
