//interface.c
//holds a method interface() which gets user input and does things based on it
#include "bargraph.h"

//#define RUN_ONLY_ONCE 1

// do the two provided strings match (assumes both strings are only 2 chars long)
static inline bool same(char * s1 , char * s2){
	if ( (s1[0] == s2[0]) && (s1[1] == s2[1]) ){
		return true;
	}
	else{
		return false;
	}
}

void *interface(void * threadid){
	extern bool write_screen;
	extern int * barvals;	
	extern int cbars;
	char  uin;
#ifndef RUN_ONLY_ONCE 
	for (;;){
#endif
		printf("> ");
		scanf("%s", &uin);
		
		/* D O   U S E R S   C O M M A N D */
		if (same(&uin , "rc")){						//	RANDOM BAR COLORS
			//rerandomize colors
			gen_colors();
		}
		else if (same (&uin, "sv")){					//	SAVE AS FILE
			write_screen = true;
			while(write_screen);//wait
		}
		else if (same(&uin, "st")){					//	SORT BARS
			qsort(barvals, cbars, sizeof(int), cmpfunc);
		}
		else if (same(&uin, "help")){
			printf("HELP PAGE:\n");
			printf("\tCommand options:\n");
			printf("\t\trc\tRerandomize Colors... change the colors of all the bars to new random values.\n");
			printf("\t\tsv\tsave graph as png\n");
		}
		else if (same(&uin, "end")){
			return threadid;
		}
		else {
			printf("%s is not a known command!\n", &uin);
		}
#ifndef RUN_ONLY_ONCE
	}
#endif
#ifdef RUN_ONLY_ONCE
	printf("Thread: %ld only ran once\n", *(long*)threadid);
	return threadid;
#endif
}
