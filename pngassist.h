
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <libpng/png.h>

//adapted from https://gist.github.com/1995eaton/1fd160fa776f31044b40

struct PNG {
  int width;
  int height;
  int depth;
  int pixel_bytes;
  png_bytep color;
  png_structp pngp;
  png_byte color_type;
  png_infop infop;
  png_byte **data;
};

int put_pixel(struct PNG *, int , int ) ;

void write_png(struct PNG *, char *) ;

void set_color(struct PNG *, png_byte , png_byte , png_byte );

int read_png(struct PNG *, char *);

void create_png(struct PNG *, int , int , png_byte );

void draw_rect(struct PNG *, int , int , int , int );

void clear_png(struct PNG*);
