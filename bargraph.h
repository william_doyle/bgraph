#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <GL/freeglut.h>	/* openGL -- used for visuals */
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <libpng/png.h>			//used to generate png when user wants to save graph
#include "pngassist.h"			//used to wrap libpng to make use easy
#include <leptonica/allheaders.h>	//used to rotate png
//first num in file is number of bars in our graph

#define OFFSET 40.0f
#define WWIDTH	700		/* window width  */
#define WHEIGHT	700		/* window height */
#define WTITLE	"GRAPH TITLE"	/* window title  */
#define CLEAR_COLOR 0,0,0,0	/* clear color	 */
#define BAR_WIDTH 50
#define SHOW_WATER_MARK 0	/* SHOW WATER MARK? */
#define DEVMODE 0		/* DEVELOPER MODE */
#define GRID_TRANS 30		/* GRID TRANSPARENCY --short */
#define WIN_START_X_POS 0	/* START POSITION OF WINDOW --X */
#define WIN_START_Y_POS 0	/* START POSITION OF WINDOW --Y */

/* G L O B A L S */
int * barvals;
int cbars;
int largestbar;
int smallestbar;
char * graphTitle;
char * xTitle;
char * yTitle;
int * reds;
int * greens;
int * blues;

bool write_screen;

void gen_colors(void);

int get_filename_index(int , char **); //find the data file and return it

void *interface(void *); // interface with the user in a seperate thread

void _update (void );

/*	METHOD:  _OUTPUT()
 *	ARGS(4): int (x pos), int (y pos), void ptr (font) , char * (desired text)
 *	DESC:    puts text to screen
 */	
void _output(int x , int y ,void * font , char * string);

void _idle (void);

/*	METHOD:  set_title()
 *	ARGS(1): char* (desired title)
 *	DESC: Simplifies title setting.. calcs title pos
 */
//static inline void set_title(char * title);
static inline void set_title(char * title){
	void * title_font = GLUT_BITMAP_TIMES_ROMAN_24;
	glColor3ub(255.0f  , 0.0f  , 0.0f  ); //set  color --red
	int xpos = glutGet( GLUT_WINDOW_WIDTH)/2;

	//calc number of width pixels in string
	int mess_width = 0; //message width 
	for (int i = 0 ; i < sizeof(title)/sizeof(char); i++){
		mess_width += glutBitmapWidth(title_font, title[i]);
	}

	/* xpos is xpos minus half the number of pixels in the image of the word */
	xpos -=  mess_width + (mess_width / 4) ;
	int ypos = glutGet( GLUT_WINDOW_HEIGHT) - OFFSET;
	_output(xpos, ypos, title_font, title);
};

static inline void set_axis(char which , char * lbl){
	if (which == 'x'){
		_output(glutGet(GLUT_WINDOW_WIDTH)/2, 1+(OFFSET/2), GLUT_BITMAP_TIMES_ROMAN_24 , lbl);
	}
	else if (which == 'y'){
		_output(1, glutGet(GLUT_WINDOW_HEIGHT)/2, GLUT_BITMAP_TIMES_ROMAN_24 , lbl);
	}
	else {
		puts("Bad option!");
	}
};

static inline void set_xaxis(char * lbl) {
	#define DEFXNAME "X-AXIS"
	glColor3ub(0.0f, 0.0f, 255.0f);// set color
	if (strcmp(lbl, "") == 0){
		lbl = DEFXNAME;
	}
	set_axis('x', lbl);
};



static inline void set_yaxis(char * lbl) {
	#define DEFYNAME "Y-AXIS"
	glColor3ub(0.0f, 0.0f, 255.0f);// set color
	if (strcmp(lbl, "") == 0){
		lbl = DEFYNAME;
	}
	set_axis('y', lbl);
};
//tutorialpoint.com
int cmpfunc (const void * a, const void * b);
