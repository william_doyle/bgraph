

#include "bargraph.h"
#include "savegraph.h"

//4.2 write call backs
void write_row_callback(png_rw_ptr val, png_uint_32 row, int pass ){
	puts("Write_row_callback called !");
}

void _update (void){
	// source -> https://stackoverflow.com/questions/13462209/opengl-draw-rectangle-outline
	// adapted code from genpfault
	// retrieved: december 31st 2019
	
	extern int * barvals;
	extern int cbars;
	extern int largestbar;
	extern int smallestbar;

	extern bool write_screen;

	extern char * graphTitle;

	extern int * reds;
	extern int * greens;
	extern int * blues;
	extern char * xTitle;
	extern char * yTitle;

	float offset = 40;
	

	/* C A L C U L A T E    Y - S C A L E R */
	int yscaler = glutGet( GLUT_WINDOW_HEIGHT);
	do {
		yscaler = yscaler - 1;
	}while (  yscaler * largestbar > ( glutGet( GLUT_WINDOW_HEIGHT ) - (offset*2) ) );
	
	
	/* C A L C U L A T E    X - S C A L E R */
	int xscaler = glutGet( GLUT_WINDOW_WIDTH);
	do {
		xscaler = xscaler - 1;
	}while ( xscaler * cbars  > glutGet( GLUT_WINDOW_WIDTH )-(offset*2)  );

	glClear(GL_COLOR_BUFFER_BIT);				//
	glMatrixMode(GL_PROJECTION);				//
	glLoadIdentity();
	
	double w = glutGet( GLUT_WINDOW_WIDTH );
	double h = glutGet( GLUT_WINDOW_HEIGHT );
	
	glOrtho( 0, w, 0, h, -1, 1);
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glTranslatef( 0.5, 0.5, 0 );
	
	/* P U T   B A R S   O N    G R A P H */
	int color_index = 0;
	int x1 , y1 , x2 , y2;
	for (int i = 0 ; i < cbars ; i++){
		x1 = (i*xscaler)+offset;
		y1 = 0+offset;

		x2 = x1 + xscaler;
		y2 = barvals[i] * yscaler + offset;

		glColor3ub( reds[i] , greens[i] , blues[i]     );
		glRectf( x1 , y1 , x2 , y2  );
	}

	/* D R A W   X   A N D   Y   A X I S */
	glColor3ub(255.0f  , 0.0f  , 0.0f  ); //set axis line color --red
	glBegin(GL_LINES);//draw x axis
		glVertex2f(offset, offset); 
		glVertex2f(w - offset, offset );
	glEnd();

	glBegin(GL_LINES);//draw y axis
		glVertex2f(offset, offset); 
		glVertex2f(offset , glutGet( GLUT_WINDOW_HEIGHT )-offset );
	glEnd();

	/* D R A W   G R I D   L I N E S */
	//drawn over bars but under text
	glEnable(GL_BLEND);//enable transparency
	glColor4ub(0.0f, 0.0f, 255.0f, GRID_TRANS);//set color to blue with transparency
#define LINE_SPACE 10
	int space = smallestbar*yscaler;//LINE_SPACE;// goal is to use adaptive value based on graph size..
	int num_hoz = (w-(offset*2))/space;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (int i = 1 /* start at 1 for multiplication reasons */ ; i <= num_hoz ; i++){
		glBegin(GL_LINES);
			glVertex2f(offset, offset+(space*i));
			glVertex2f(w-offset, offset+(space*i));
		glEnd();
	}
	glDisable(GL_BLEND);//disable transparency

	/* L A B E L   T O P   X   A N D   Y   V A L U E S */
	char str[11];

	sprintf(str , "%d", largestbar);//label max of y 
	_output((int)( offset), (int )(largestbar*yscaler+offset) , GLUT_BITMAP_TIMES_ROMAN_24, str);

	sprintf(str , "%d", cbars);//label max of x 
	_output((int)((cbars*xscaler)+offset), (int)(offset-24) , GLUT_BITMAP_TIMES_ROMAN_24, str);

	/* P U T    G R A P H I C L   T I T L E  */
	set_title(graphTitle);	
	//pux x and y axis labels
	set_xaxis(xTitle);	
	set_yaxis(yTitle);	

	if (write_screen == true){
		savegraph((int)w, (int)h );
		write_screen = false;									//notify interface thread we have done what we needed to
	}

	glutSwapBuffers();

}
