#include "bargraph.h"


//color related functions
//

//generate the random colors
void gen_colors(void){
	extern int * reds;
	extern int * greens;
	extern int * blues;

	extern int cbars;

	for (int i = 0; i < cbars; i++){
#define MAX 250
#define MIN 10
		blues[i] = rand() % (MAX-MIN)+MIN;
		greens[i] = rand() % (MAX-MIN)+MIN;
		reds[i] = rand() % (MAX-MIN)+MIN;
	}
}
