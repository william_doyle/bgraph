#include "bargraph.h"

/*	METHOD:  _OUTPUT()
 *	ARGS(4): int (x pos), int (y pos), void ptr (font) , char * (desired text)
 *	DESC:    puts text to screen
 */	
void _output(int x , int y ,void * font , char * string){
	glRasterPos2f(x, y);
	int len, i ;
	len = strlen(string);
	for (i = 0 ; i < len; i++){
		glutBitmapCharacter(font, string[i]);
	}
}//close _output


/*	METHOD:  set_title()
 *	ARGS(1): char* (desired title)
 *	DESC: Simplifies title setting.. calcs title pos
 */
//static inline void set_title(char * title){
//	void * title_font = GLUT_BITMAP_TIMES_ROMAN_24;
//	glColor3ub(255.0f  , 0.0f  , 0.0f  ); //set axis line color --red
//	int xpos = glutGet( GLUT_WINDOW_WIDTH)/2;

	//calc number of width pixels in string
//	int mess_width = 0; //message width 
//	for (int i = 0 ; i < sizeof(title)/sizeof(char); i++){
//		mess_width += glutBitmapWidth(title_font, title[i]);
//	}

	/* xpos is xpos minus half the number of pixels in the image of the word */
//	xpos -=  mess_width + (mess_width / 4) ;
//	int ypos = glutGet( GLUT_WINDOW_HEIGHT) - OFFSET;
//	_output(xpos, ypos, title_font, title);
//}
