#include "bargraph.h"

#define INTHREAD true
int cmpfunc (const void * a, const void * b){
	return (*(int*)a-*(int*)b);
}

void _idle (void) {
	glutPostRedisplay();	//marks current window as needing to be redisplayed -- opengl.org/resources
}

int main (int argc, char ** argv) {
	extern int * barvals;		// ARRAY HOLDS THE HEIGHTS OF THE BARS  
	extern int * reds;			// ARRAY HOLDS THE RED VALUE FOR THE COLORS OF THE BARS 
	extern int * greens;		// ARRAY HOLDS THE GREEN VALUE FOR THE COLORS OF THE BARS
	extern int * blues;			// ARRAY HOLDS THE BLUE VALUE FOR THE COLORS OF THE BARS 
	extern int cbars;			// HOLDS THE NUMBER OF BARS
	extern bool write_screen;	// BOOL: TRUE WHEN USER COMMANDS TO SAVE GRAPH AS IMAGE.. THEN RETURNS TO FALSE 
	write_screen = false;		// SET TO FALSE AS DEFAULT 
	extern char * graphTitle;	// STRING HOLDS TITLE OF GRAPH --PROVIDED VIA ARGS 
	extern char * xTitle;		// TITLE FOR X AXIS 
	extern char * yTitle; 
	extern int smallestbar;
	extern int largestbar;  

	xTitle = "";
	yTitle = "";

	if (argc == 1){
		fprintf(stderr, "No Args Specified! Please See Documentation! Quiting!\n");
		exit(EXIT_FAILURE);
	//	goto END_OF_MAIN;
	}
	
	int f_index = get_filename_index(argc, argv);
	if (f_index == -1){/* Handle errors */
		fprintf(stderr, "No Filename specified! Please See Docs! Quiting!\n");
		exit(EXIT_FAILURE);
	//	goto END_OF_MAIN;;
	}

	FILE * theFile = fopen( argv[f_index], "r");
	
	/* G R O W    T H E     A R R A Y */
	fscanf(theFile, "%d", &cbars);							//get number of bars from standard input
	barvals =  malloc((cbars * sizeof(int)) );				//grow
	reds =     malloc(cbars*sizeof(int));
	blues =    malloc(cbars*sizeof(int));
	greens =   malloc(cbars*sizeof(int));
	
	if ((!barvals)  || (!reds) || (!blues) || (!greens) ){	//check malloc was successful
		fprintf(stderr, "1 or more mallocs failed in main!\n");
		if (theFile){
			fclose(theFile);
		}
		exit(EXIT_FAILURE);
	}

	srand(time(NULL) * getpid());							//seed random number generater with current time and process id
	
	/*P O P U L A T E   T H E   A R R A Y */
	for (int i = 0; i < cbars; i++){
		fscanf( theFile , "%d", &barvals[i] );	
	}
	
	/* P O P U L A T E   C O L O R   A R R A Y */
	gen_colors();

	/* D I S P L A Y   T H E   D A T A */
	for (int i = 0; i < cbars; i++){
		printf("%d " , barvals[i] );
	}	
	printf("\n");											// put a new line

	/* C H O O S E   S C A L E */
	smallestbar = barvals[0];
	largestbar  = barvals[0];
	for (int i = 0; i < cbars; i++){
		if (barvals[i] < smallestbar){
			smallestbar = barvals[i];
		}
		if (barvals[i] > largestbar){
			largestbar = barvals[i];
		}
	}

	/* I N T E R F A C E   P T H R E A D   C O D E */
	pthread_t  USER_IN;
	if (INTHREAD){
		pthread_create(&USER_IN, NULL , interface , NULL);
	}

	/* O P E N G L    S T U F F */
	glutInit(&argc, argv);									//initilize glut library
	glutInitDisplayMode(GLUT_SINGLE);						//set initial display mode
	glutInitWindowPosition(WIN_START_X_POS, WIN_START_Y_POS);				//set initial window position
	glutInitWindowSize(WWIDTH, WHEIGHT);					//set width and height
	/* when user presses x come back to main() */
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);	

	if (argc >= 2){
		graphTitle = argv[1];								// get provided title
	}
	else{
		graphTitle = WTITLE;								// get default title
	}
	glutCreateWindow(graphTitle);							// create window with title

	glClearColor(255.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f, 1.0f);	

	glClear(GL_COLOR_BUFFER_BIT);			
	glutDisplayFunc( _update );								//what to do on refreash
	glutIdleFunc( _idle );									//what to do when not refreashing
	glutMainLoop();					

TIDY_EXIT:
	fclose(theFile);										// CLOSE THE FILE
	/* F R E E   A L L O C A T E D   M E M O R Y */
	if (barvals){
		free(barvals);
	}
	if (reds){
		free(reds);
	}
	if (greens){
		free(greens);
	}
	if (blues){
		free(blues);
	}
END_OF_MAIN:
#ifdef DEBUG
	printf("Reached bottom of main\n");
#endif
	exit(EXIT_SUCCESS);
}
