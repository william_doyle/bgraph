
#pragma once
#include "bargraph.h"

#define COLPOS (i*h)*3

static inline int savegraph (int w, int h){
	unsigned char *pixels =  malloc (w * h * sizeof(char)*3);					//allocate memory to hold pixels
	if (!pixels){
		printf("malloc for \"pixels\" failed!\n");
	}
	glReadPixels(0 , 0 , w , h , GL_RGB , GL_UNSIGNED_BYTE, pixels);			//opengl get pixels
			
	char * FILENAME;									//holds filename of image to save
	printf("\nEnter File Name: ");								//ask user for filename
	scanf("%s", FILENAME);									//get filename from user

	struct PNG png;										//create png object -- assistpng 
	create_png(&png, w ,h , PNG_COLOR_TYPE_RGB);						//construct png object 
	for (int i = 0 ; i < w ; i++ ){							/* loop to put pixels into png object  */
		for (int k = 0 ; k < h*3 ; k+=3){
			set_color(&png, *(pixels + COLPOS  + k) ,				/* set color of pixel to hold sub pixel values  */
				      	*(pixels + COLPOS  +k+1), 
				       	*(pixels + COLPOS + k+2));
			put_pixel(&png, i , k/3);						/* place pixel into buffer */
			put_pixel(&png, i , (k+1)/3);						/* next sub pixel */
			put_pixel(&png, i , (k+2)/3);						/* last sub pixel */
		}//close inner for
	}//close outer for 

	/* save the data as a png */
	write_png(&png, FILENAME);								//save image as png with filename
	
	PIX * _image;										//create PIX object
       	_image  = pixRead(FILENAME);								//load the new  png file into pix object (ew reading what we just wrote)
	_image = pixRotate90(_image, 1);							//rotate 90 degrees
	_image = pixRotate90(_image, 1);							//rotate 90 degrees again	--bad style 
	_image = pixRotate90(_image, 1);							//rotate 90 degrees once more   --bad style


	pixWrite(FILENAME, _image, 1);								//over write with rotated image
//	pixDestroy(&_image);									//destroy image (free memory)		
	free(pixels);										//free pixels	
	clear_png(&png);	
};
